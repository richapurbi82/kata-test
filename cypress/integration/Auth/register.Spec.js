context('As User I want to Register', () => {
    beforeEach(() => {
        cy.visit(Cypress.config().base_url)
        cy.wait(500)
        cy.get('a[class="login"]').contains('Sign in').click()
        
    })
   

    it('Sucess Register', () => {
        cy.get('input[id="email_create"]').type('richa@getnada.com')
        cy.get('button[id="SubmitCreate"]').click()
        //Personal Information
        cy.get('input[id="id_gender1"]').check()
        cy.get('input[id="customer_firstname"]').type('Richa')
        cy.get('input[id="customer_lastname"]').type('Purbinawati')
        cy.get('input[id="passwd"]').type('rahasia')
        cy.get('select[id="days"]').select('30').should('have.value', '30')
        cy.get('select[id="months"]').select('July').should('have.value', '7')
        cy.get('select[id="years"]').select('1997').should('have.value', '1997')
        // Address
        //cy.get('input[id="firstname"]').type('Richa')
        //cy.get('input[id="lastname"]').type('Purbinawati')
        cy.get('input[id="address1"]').type('JL. Kol Sugiono 134D')
        cy.get('input[id="city"]').type('Malang')
        cy.get('select[id="id_state"]').select('Arizona').should('have.value', '3') 
        cy.get('input[id="postcode"]').type('65144')
        //cy.get('select[id="id_country"]').select('United States').should('have.value', '21')
        cy.get('input[id="phone_mobile"]').type('081330311957')
        cy.get('button[id="submitAccount"]').click()
        cy.get('p').contains('Welcome to your account. Here you can manage all of your personal information and orders.')

    })

   
})