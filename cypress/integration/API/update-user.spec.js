describe('As a user, I want to Update user data', () => {
    it('PUT-Update valid user',()=>{
        var user = {
            "name": "Richa Purbinawati",
            "job": "QA"
        }

        cy.request('PUT','api/users/2',user).then((response)=>{
            expect(response.status).equal(200)
            expect(response.body.name).equal(user.name)
            expect(response.body.job).equal(user.job)

        })
        
    })

})